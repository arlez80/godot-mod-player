# Godot MOD Player

[![ko-fi](https://ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/E1E44AWTA)

Software mod/xm player library for Godot Engine 3.5 later.
This player is embeddable for your Godot projects!

- mod is a sound file format that used primarily in old computer Amiga.
- xm is a sound file format that extended mod.

## Demo

You can download demo app: https://bitbucket.org/arlez80/godot-mod-player/downloads/demo.zip

### Videos

- [mod](https://www.youtube.com/watch?v=K8NQ1VjClI0)
- [xm](https://www.youtube.com/watch?v=1q93jjYhYuk)

## Issues

[see issues](https://bitbucket.org/arlez80/godot-mod-player/issues?status=new&status=open)

## License

MIT License

## Author

* @arlez80 あるる / きのもと 結衣 ( Yui Kinomoto )
